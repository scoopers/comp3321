%{
  #include "parser.tab.h"
  int yylval;
%}
%%
[a-zA-Z] {return ID;}
[0-9]+ {return INT;}
"int" {return INTID;}
"while" {return WHILEID;}
"if" {return IFID;}
"else" {return ELSEID;}
"+" {return ADD;}
"-" {return SUB;}
"*" {return MUL;}
"/" {return DIV;}
"==" {return DEQ;}
"=" {return EQ;}
"(" {return OP;}
")" {return CP;}
"{" {return OCB;}
"}" {return CCB;}
";" {return SC;}
\n {return EOL;}
"//".* {;}
[ \t] {;}
%%
