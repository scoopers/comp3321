%{
  #include <stdio.h>
  int vars[56];
%}

%token ID
%token INT
%token INTID WHILEID IFID ELSEID
%token ADD SUB MUL DIV
%token EQ DEQ
%token OP CP OCB CCB
%token SC
%token EOL

%%
root:
  | root EOL {printf("end\n");}
  ;
%%

main(int argc, char **argv) {
  yyparse();
}

yyerror(char *s) {
  fprintf(stderr, "error: %s\n", s);
}
